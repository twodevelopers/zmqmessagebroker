MessageBroker
========================

Requirements
------------

  *     The MessageBroker is only supported on PHP 5.3.3 and up
  *     You need already installed zeroMQ

Installation
------------

The best way to install the MessageBroker is to use composer, by using

```js
    php composer.phar required zeichen32/zmq-message-broker
```

Usage
------------

First we need to create a job type. The job type represents you Job like "Make thumbnail".

```php

    class ThumbnailJob extends \Zeichen32\Zmq\MessageBroker\Job\AbstractJob {

        /**
         * Return job type
         *
         * @return string
         */
        public function getType() {
            return 'MAKE_THUMBNAIL_JOB';
        }

        /**
         * Return true if job is ready to work
         *
         * @return bool
         */
        public function isReady() {
           return true;
        }
    }
```

Second we need a worker type. A worker type represents your worker on server side

```php

    class ThumbnailWorkerType extends \Zeichen32\Zmq\MessageBroker\Worker\AbstractWorker {

        /**
         * Return Worker type
         *
         * @return string
         */
        public function getType() {
            return 'THUMBNAIL_WORKER';
        }

        /**
         * Check if a job is supported by worker
         *
         * @param \Zeichen32\Zmq\MessageBroker\Job\JobInterface $job
         * @return bool
         */
        public function support(\Zeichen32\Zmq\MessageBroker\Job\JobInterface $job) {

            // If your worker support this type of job you must return true
            return ($job->getType() == 'MAKE_THUMBNAIL_JOB');
        }
    }
```

The last class we need to create, is our worker

```php

        class ThumbnailWorker extends \Zeichen32\Zmq\Worker\AbstractWorker {

        /**
         * Override this method to execute custom commands
         *
         * @param $command
         * @param \ZMQSocket $messageBroker
         * @return mixed
         */
        protected function handleCommand($command, \ZMQSocket $messageBroker)
        {
            // Most of default methods are already implement. If you need some custom
            // commands, you can handle this commands in is method
            return false;
        }

        /**
         * Override this method to handle a job request
         *
         * @param array $data
         * @param \ZMQSocket $messageBroker
         * @return bool
         */
        protected function handleJobCommand(array $data, \ZMQSocket $messageBroker)
        {
            // In this method, the work will be done
            // Return true if the job was successfully run
            // Return false if the job has been failed

            for($i = 0; $i < 10; $i++) {
                sleep(1);
            }

            return true;
        }

        public function getType()
        {
            return 'THUMBNAIL_WORKER';
        }
    }
```

At least we must configure our message broker and worker and run its in the console

```php

    // Message Broker

    $context = new \ZMQContext();

    $frontend = new \ZMQSocket($context, \ZMQ::SOCKET_ROUTER);
    $backend = new \ZMQSocket($context, \ZMQ::SOCKET_ROUTER);

    // Configure ip adresses for clients and worker
    $config = array(
        'ipAddressFrontend' => '127.0.0.1',
        'ipAddressBackend'  => '127.0.0.1',
    );

    $broker = new \Zeichen32\Zmq\MessageBroker\MessageBroker($frontend, $backend, $config);

    // Add you worker types as closure or factory class
    $broker->addWorkerType('THUMBNAIL_WORKER', function($workerAddress) {
        return new ThumbnailWorkerType($workerAddress);
    });

    // Add your job types as closure or factory class
    $broker->addJobType('MAKE_THUMBNAIL_JOB', function($clientAddress) {
       return new ThumbnailJob($clientAddress);
    });

    // Set a example logger. You can leave this...
    // You can add classes with implements LoggerInterface or callback function
    $broker->setLogger(new \Zeichen32\Zmq\Logger\EchoLogger());

    // Start message broker
    $broker->run();

```

```php

    // Worker
    $worker = new ThumbnailWorker(array('server' => '127.0.0.1'));

    // Connect worker to message broker
    $worker->connect();

```

If you had started the message broker and worker in the console, you can start a job in your application by use the static
client method:

```php
    \Zeichen32\Zmq\Client\Client::callMessageBroker('MAKE_THUMBNAIL_JOB');
```

For a working example have a look at "examples" folder
