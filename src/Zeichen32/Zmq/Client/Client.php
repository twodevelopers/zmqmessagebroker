<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 08.08.13
 * Time: 09:26
 */

namespace Zeichen32\Zmq\Client;


class Client
{

    protected $ip;
    protected $port;

    function __construct($ip = '127.0.0.1', $port = 4444)
    {
        $this->ip = $ip;
        $this->port = $port;
    }

    /**
     * Sent job to message broker
     *
     * @param $job
     * @param array $data
     */
    public function call($job, array $data = array())
    {
        self::callMessageBroker($job, $data, $this->ip, $this->port);
    }

    /**
     * Static method to sent quick and dirty a job to message broker
     *
     * @param $job
     * @param array $data
     * @param string $ip
     * @param int $port
     */
    public static function callMessageBroker($job, array $data = array(), $ip = '127.0.0.1', $port = 4444)
    {

        $context = new \ZMQContext();
        $client = new \ZMQSocket($context, \ZMQ::SOCKET_REQ);
        $client->connect(sprintf('tcp://%s:%d', $ip, $port));

        $client->send($job, \ZMQ::MODE_SNDMORE);
        $client->send('', \ZMQ::MODE_SNDMORE);
        $client->send(json_encode($data));
    }
} 