<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 08.08.13
 * Time: 09:26
 */

namespace Zeichen32\Zmq\Worker;


interface WorkerInterface
{
    const WORKER_COMMAND_SUBSCRIBE = 'SUBSCRIBE';
    const WORKER_COMMAND_SHUTDOWN = 'SHUTDOWN';
    const WORKER_COMMAND_PING = 'PING';
    const WOKRER_COMMAND_HANDLEJOB = 'HANDLEJOB';
    const WORKER_COMMAND_STATUS = 'STATUS';

    const BROKER_RESPONSE_UNSUBSCRIBE = 'UNSUBSCRIBE';
    const BROKER_RESPONSE_PONG = 'PONG';
    const BROKER_RESPONSE_JOB_COMPLETE = 'JOBCOMPLETE';
    const BROKER_RESPONSE_JOB_FAILED = 'JOBFAILED';
    const BROKER_RESPONSE_JOB_SUCCESS = 'JOBSUCCESS';
    const BROKER_RESPONSE_STATUS_UPDATE = 'WORKERSTATUS';

    public function getType();

    public function connect();
} 