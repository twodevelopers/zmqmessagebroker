<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 08.08.13
 * Time: 09:26
 */

namespace Zeichen32\Zmq\Worker;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zeichen32\Zmq\LoggableInterface;
use Zeichen32\Zmq\Logger\LoggerInterface;

abstract class AbstractWorker implements WorkerInterface, LoggableInterface
{

    protected $options;
    protected $connected = false;
    protected $logger;
    protected $startTime;

    function __construct(array $options = array())
    {
        $resolver = new OptionsResolver();
        $this->setDefaultOptions($resolver);
        $this->options = $resolver->resolve($options);
        $this->startTime = new \DateTime();
    }

    public function setLogger($logger)
    {
        if (is_callable($logger) || $logger instanceof LoggerInterface) {
            $this->logger = $logger;
        }
    }

    public function log($message, $type = 'debug')
    {
        if (is_callable($this->logger)) {
            call_user_func_array($this->logger, array($message, $type));
        }
    }

    protected function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'server',
        ));

        $resolver->setOptional(array(
            'port',
        ));

        $resolver->setDefaults(array(
            'port' => 5555
        ));

        $resolver->setAllowedTypes(array(
            'port' => 'integer',
        ));
    }

    protected function getServerAddress()
    {
        return sprintf('tcp://%s:%d', $this->options['server'], $this->options['port']);
    }

    public function connect()
    {

        // Create new Context
        $context = new \ZMQContext();
        $messageBroker = $context->getSocket(\ZMQ::SOCKET_REQ);

        // Generate Worker Address
        $identity = sprintf("%04X", rand(0, 0x10000));
        $messageBroker->setSockOpt(\ZMQ::SOCKOPT_IDENTITY, $identity);

        // Connect to Messagebroker
        $messageBroker->connect($this->getServerAddress());
        $messageBroker->send(self::WORKER_COMMAND_SUBSCRIBE, \ZMQ::MODE_SNDMORE);
        $messageBroker->send("", \ZMQ::MODE_SNDMORE);
        $messageBroker->send($this->getType());

        $this->connected = true;

        // Handling loop
        while (true) {
            $command = $messageBroker->recv();

            switch ($command) {
                // Worker shutdown
                case self::WORKER_COMMAND_SHUTDOWN:
                    $this->onShutdown($messageBroker);
                    return;

                // Worker ping-pong
                case self::WORKER_COMMAND_PING:
                    $this->onPing($messageBroker);
                    break;

                case self::WOKRER_COMMAND_HANDLEJOB:
                    $this->handleJob($messageBroker);
                    break;

                case self::WORKER_COMMAND_STATUS:
                    $this->onStatus($messageBroker);
                    break;

                // Handle custom commands
                default:
                    $this->emptyFrame($messageBroker);
                    $this->handleCommand($command, $messageBroker);
                    break;
            }
        }
    }

    protected function onPing(\ZMQSocket $messageBroker)
    {
        $this->log('Receive Ping, Send Pong...');
        $messageBroker->send(self::BROKER_RESPONSE_PONG);
    }

    protected function onStatus(\ZMQSocket $messageBroker) {

        $data = array(
            'memory_usage' => $this->getFormatedRamUsage(),
            'memory_peak'  => $this->getFormatedRamPeak(),
            'runtime'      => $this->formatDateDiff($this->startTime),
            'type'         => $this->getType(),
            'ip'           => gethostname(),
            'os'           => PHP_OS,
            'extras'     => php_uname(),
        );

        $this->log('Send worker infos...');

        foreach($data as $key=>$value) {
            $this->log(sprintf('%s : %s', $key, $value));
        }

        $messageBroker->send(self::BROKER_RESPONSE_STATUS_UPDATE, \ZMQ::MODE_SNDMORE);
        $messageBroker->send("", \ZMQ::MODE_SNDMORE);
        $messageBroker->send(json_encode($data));
    }

    protected function onShutdown(\ZMQSocket $messageBroker)
    {
        $this->log('Receive Shutdown... Goodbye...');
        $messageBroker->send(self::BROKER_RESPONSE_UNSUBSCRIBE);
    }

    protected function handleJob(\ZMQSocket $messageBroker)
    {
        $this->emptyFrame($messageBroker);

        $data = $messageBroker->recv();
        $data = json_decode($data, true);

        $this->log('Receive new job...');

        $jobResponse = $this->handleJobCommand($data, $messageBroker);

        $messageBroker->send(self::BROKER_RESPONSE_JOB_COMPLETE, \ZMQ::MODE_SNDMORE);
        $messageBroker->send("", \ZMQ::MODE_SNDMORE);

        if (true === $jobResponse) {
            $this->log('Job was successfully...');
            $messageBroker->send(self::BROKER_RESPONSE_JOB_SUCCESS);
        } else {
            $this->log('Job has failed!', 'error');
            $messageBroker->send(self::BROKER_RESPONSE_JOB_FAILED);
        }
    }

    /**
     * Receive and check a empty frame
     *
     * @param $socket
     * @throws \LogicException
     */
    protected function emptyFrame($socket)
    {
        $empty = $socket->recv();

        if (!empty($empty)) {
            throw new \LogicException('Expected empty frame, but receive none empty frame');
        }
    }

    protected function formatImageSize($bytes, $force_unit = NULL, $format = NULL, $si = TRUE) {

        // Format string
        $format = ($format === NULL) ? '%01.2f %s' : (string) $format;

        // IEC prefixes (binary)
        if ($si == FALSE OR strpos($force_unit, 'i') !== FALSE)
        {
            $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
            $mod   = 1024;
        }
        // SI prefixes (decimal)
        else
        {
            $units = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
            $mod   = 1000;
        }

        // Determine unit to use
        if (($power = array_search((string) $force_unit, $units)) === FALSE)
        {
            $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
        }

        return sprintf($format, $bytes / pow($mod, $power), $units[$power]);
    }

    protected function getFormatedRamUsage() {
        return $this->formatImageSize(memory_get_usage(true));
    }

    protected function getFormatedRamPeak() {
        return $this->formatImageSize(memory_get_peak_usage(true));
    }

    public function formatDateDiff($start, $end=null) {
        if(!($start instanceof \DateTime)) {
            $start = new \DateTime($start);
        }

        if($end === null) {
            $end = new \DateTime();
        }

        if(!($end instanceof \DateTime)) {
            $end = new \DateTime($start);
        }

        $interval = $end->diff($start);
        $doPlural = function($nb,$str){return $nb>1?$str.'s':$str;}; // adds plurals

        $format = array();
        if($interval->y !== 0) {
            $format[] = "%y ".$doPlural($interval->y, "year");
        }
        if($interval->m !== 0) {
            $format[] = "%m ".$doPlural($interval->m, "month");
        }
        if($interval->d !== 0) {
            $format[] = "%d ".$doPlural($interval->d, "day");
        }
        if($interval->h !== 0) {
            $format[] = "%h ".$doPlural($interval->h, "hour");
        }
        if($interval->i !== 0) {
            $format[] = "%i ".$doPlural($interval->i, "minute");
        }
        if($interval->s !== 0) {
            if(!count($format)) {
                return "less than a minute ago";
            } else {
                $format[] = "%s ".$doPlural($interval->s, "second");
            }
        }

        // We use the two biggest parts
        if(count($format) > 1) {
            $format = array_shift($format)." and ".array_shift($format);
        } else {
            $format = array_pop($format);
        }

        // Prepend 'since ' or whatever you like
        return $interval->format($format);
    }

    /**
     * Override this method to execute commands
     *
     * @param $command
     * @param \ZMQSocket $messageBroker
     * @return mixed
     */
    abstract protected function handleCommand($command, \ZMQSocket $messageBroker);

    /**
     * Override this method to handle a job request
     *
     * @param array $data
     * @param \ZMQSocket $messageBroker
     * @return bool
     */
    abstract protected function handleJobCommand(array $data, \ZMQSocket $messageBroker);
}