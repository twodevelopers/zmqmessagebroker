<?php
/**
 * Created by PhpStorm.
 * User: jens
 * Date: 30.09.13
 * Time: 17:07
 */

namespace Zeichen32\Zmq\Worker;


class LogMonitorWorker extends AbstractWorker {

    /**
     * Override this method to execute commands
     *
     * @param $command
     * @param \ZMQSocket $messageBroker
     * @return mixed
     */
    protected function handleCommand($command, \ZMQSocket $messageBroker)
    {
        if($command == 'LOG') {

            $data = $messageBroker->recv();
            $data = json_decode($data, true);

            if(!empty($data) && is_array($data)) {
                foreach($data as $message) {
                    $type = (isset($message['type']) && $message['type'] == 'error') ? 'error' : 'debug';
                    $this->log(sprintf('[MessageBroker] : %s', $message['msg']), $type);
                }
            }

            $messageBroker->send('LOG');

            return true;
        }

        return false;
    }

    /**
     * Override this method to handle a job request
     *
     * @param array $data
     * @param \ZMQSocket $messageBroker
     * @return bool
     */
    protected function handleJobCommand(array $data, \ZMQSocket $messageBroker)
    {
        return false;
    }

    public function getType()
    {
        return 'LOG_WORKER_TYPE';
    }

} 