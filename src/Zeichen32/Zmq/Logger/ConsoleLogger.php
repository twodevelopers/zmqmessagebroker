<?php
/**
 * Created by PhpStorm.
 * User: jens
 * Date: 12.08.13
 * Time: 15:37
 */

namespace Zeichen32\Zmq\Logger;

use Symfony\Component\Console\Output\OutputInterface;

class ConsoleLogger implements LoggerInterface
{

    protected $output;

    function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    function __invoke($message, $type)
    {
        if ($type == 'error') {
            $this->output->writeln(sprintf('<fg=red>[%s][%s]</fg=red> : %s', date('Y-m-d H:i:s'), $type, $message));
        } else {
            $this->output->writeln(sprintf('<fg=green>[%s][%s]</fg=green> : %s', date('Y-m-d H:i:s'), $type, $message));
        }
    }
}
