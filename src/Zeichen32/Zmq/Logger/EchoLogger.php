<?php
/**
 * Created by PhpStorm.
 * User: jens
 * Date: 12.08.13
 * Time: 15:35
 */

namespace Zeichen32\Zmq\Logger;

class EchoLogger implements LoggerInterface
{
    function __invoke($message, $type)
    {
        printf('[%s][%s]: %s' . PHP_EOL, date('Y-m-d H:i:s'), $type, $message);
    }
}
