<?php
/**
 * Created by PhpStorm.
 * User: jens
 * Date: 14.08.13
 * Time: 09:20
 */

namespace Zeichen32\Zmq\Logger;

interface LoggerInterface
{
    function __invoke($message, $type);
} 