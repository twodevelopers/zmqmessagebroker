<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 08.08.13
 * Time: 09:26
 */

namespace Zeichen32\Zmq\MessageBroker\Job;

interface JobInterface
{

    /**
     * Return job type
     * @return string
     */
    public function getType();

    /**
     * Set Client id
     *
     * @param $client_address
     * @return string
     */
    public function setClientAddress($client_address);

    /**
     * Get client id
     *
     * @return string
     */
    public function getClientAddress();

    /**
     * Return true if job is ready to work
     *
     * @return bool
     */
    public function isReady();

    /**
     * Add one count to ttl
     *
     * @return void
     */
    public function addTTL();

    /**
     * Return current ttl
     * @return integer
     */
    public function getTTL();

    /**
     * Set ttl
     *
     * @param integer $ttl
     * @return void
     */
    public function setTTL($ttl);

    /**
     * @return bool
     */
    public function hasExceededTTL();

    /**
     * @return array
     */
    public function getJobData();

    /**
     * @param array $jobData
     * @return void
     */
    public function setJobData(array $jobData);
}
