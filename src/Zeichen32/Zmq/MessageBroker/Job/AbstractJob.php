<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 08.08.13
 * Time: 09:26
 */

namespace Zeichen32\Zmq\MessageBroker\Job;

use Zeichen32\Zmq\LoggableInterface;
use Zeichen32\Zmq\Logger\LoggerInterface;

abstract class AbstractJob implements JobInterface, LoggableInterface
{

    protected $clientAddress;
    protected $ttl = 1;
    protected $jobData;
    protected $logger;

    function __construct($clientAddress)
    {
        $this->clientAddress = $clientAddress;
    }

    public function setLogger($logger)
    {
        if (is_callable($logger) || $logger instanceof LoggerInterface) {
            $this->logger = $logger;
        }
    }

    public function log($message, $type = 'debug')
    {
        if (is_callable($this->logger)) {
            call_user_func_array($this->logger, array($message, $type));
        }
    }


    /**
     * Set Client id
     *
     * @param $client_address
     * @return string
     */
    public function setClientAddress($client_address)
    {
        $this->clientAddress = $client_address;
    }

    /**
     * Get client id
     *
     * @return string
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * Add one count to ttl
     *
     * @return void
     */
    public function addTTL()
    {
        $this->ttl++;
    }

    /**
     * Return current ttl
     * @return integer
     */
    public function getTTL()
    {
        return $this->ttl;
    }

    /**
     * Set ttl
     *
     * @param integer $ttl
     * @return void
     */
    public function setTTL($ttl)
    {
        $this->ttl = $ttl;
    }

    /**
     * @return bool
     */
    public function hasExceededTTL()
    {
        return (bool)($this->ttl > 3);
    }

    /**
     * @return array
     */
    public function getJobData()
    {
        return $this->jobData;
    }

    /**
     * @param array $jobData
     * @return void
     */
    public function setJobData(array $jobData)
    {
        $this->jobData = $jobData;
    }


}
