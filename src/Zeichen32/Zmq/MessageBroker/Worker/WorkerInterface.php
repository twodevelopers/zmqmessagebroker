<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 08.08.13
 * Time: 09:26
 */

namespace Zeichen32\Zmq\MessageBroker\Worker;


use Zeichen32\Zmq\MessageBroker\Job\JobInterface;

interface WorkerInterface
{

    /**
     * Return Worker type
     *
     * @return string
     */
    public function getType();

    /**
     * Return worker identifier
     *
     * @return string
     */
    public function getWorkerAddress();

    /**
     * Return last ping time
     *
     * @return \DateTime
     */
    public function getLastPing();

    /**
     * Set last ping to current time
     *
     * @return void
     */
    public function setLastPing();

    /**
     * Get current worker state
     *
     * @return bool
     */
    public function isWorking();

    /**
     * Set worker state to working
     */
    public function setWorking();

    /**
     *  Set worker state to not working
     */
    public function setNotWorking();

    /**
     * Set worker in waiting mode for next ping
     * @return void
     */
    public function setWaitingForNextPing();

    /**
     * Get worker ping state
     *
     * @return bool
     */
    public function isWaitingForNextPing();

    /**
     * Check if a job is supported by worker
     *
     * @param JobInterface $job
     * @return bool
     */
    public function support(JobInterface $job);

    /**
     * Shutdown the worker
     *
     * @param \ZMQSocket $socket
     * @return void
     */
    public function shutdown(\ZMQSocket $socket);

    /**
     * @return bool
     */
    public function hasCloseRequest();

    /**
     * @return void
     */
    public function closeRequest();

    /**
     * Called when worker has unsubscribe from router
     *
     * @return void
     */
    public function onUnsubscribe();

    /**
     * @return bool
     */
    public function hasJob();

    /**
     * @param JobInterface $job
     * @return void
     */
    public function setCurrentJob(JobInterface $job);

    /**
     * @return void
     */
    public function removeCurrentJob();

    /**
     * @return JobInterface
     */
    public function getCurrentJob();

    /**
     * Get all posible commands for worker.
     *
     * array('HELLOWORLD' => 'onHelloWorldCommand')
     *
     * @return array
     */
    public function getCommands();

    /**
     * Sent ping to worker
     *
     * @param \ZMQSocket $socket
     * @return bool
     */
    public function ping(\ZMQSocket $socket);

    /**
     * Return true, if worker support status command
     *
     * @return bool
     */
    public function canReceiveStatusCommand();

    /**
     * Send a status request
     *
     * @param \ZMQSocket $socket
     * @return bool
     */
    public function requestStatus(\ZMQSocket $socket);

    /**
     * @param \ZMQSocket $socket
     * @param JobInterface $job
     * @return void
     */
    public function handleJob(\ZMQSocket $socket, JobInterface $job);
}
