<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 08.08.13
 * Time: 12:00
 */

namespace Zeichen32\Zmq\MessageBroker\Worker;


use Zeichen32\Zmq\LoggableInterface;
use Zeichen32\Zmq\Logger\LoggerInterface;
use Zeichen32\Zmq\MessageBroker\Job\JobInterface;
use Zeichen32\Zmq\MessageBroker\MessageBroker;

abstract class AbstractWorker implements WorkerInterface, LoggableInterface
{

    private $workerAddress;

    protected $isWorking = false;
    protected $currentJob = null;
    protected $lastPing;
    protected $waitingForPing = false;
    protected $closeRequest = false;
    protected $logger;
    protected $status = array();

    function __construct($workerAddress)
    {
        $this->workerAddress = $workerAddress;
        $this->lastPing = new \DateTime();
    }

    /**
     * Get all posible commands for worker.
     *
     * array('HELLOWORLD' => 'onHelloWorldCommand')
     *
     * @return array
     */
    public function getCommands()
    {
        return array(
            MessageBroker::WORKER_RESPONSE_JOB_COMPLETE => 'onJobComplete',
            MessageBroker::WORKER_RESPONSE_STATUS_UPDATE => 'onStatusUpdate',
        );
    }


    public function setLogger($logger)
    {
        if (is_callable($logger) || $logger instanceof LoggerInterface) {
            $this->logger = $logger;
        }
    }

    public function log($message, $type = 'debug')
    {
        if (is_callable($this->logger)) {
            call_user_func_array($this->logger, array($message, $type));
        }
    }

    /**
     * @return string
     */
    public function getWorkerAddress()
    {
        return $this->workerAddress;
    }

    /**
     * @return \DateTime
     */
    public function getLastPing()
    {
        return $this->lastPing;
    }

    public function setLastPing()
    {
        $this->lastPing = new \DateTime();
        $this->waitingForPing = false;
    }

    public function ping(\ZMQSocket $socket)
    {

        $this->waitingForPing = true;

        $socket->send($this->getWorkerAddress(), \ZMQ::MODE_SNDMORE);
        $socket->send("", \ZMQ::MODE_SNDMORE);
        $socket->send(MessageBroker::WORKER_COMMAND_PING);

        return true;
    }

    /**
     * Get current worker state
     *
     * @return bool
     */
    public function isWorking()
    {
        return $this->isWorking;
    }

    /**
     * Set worker state to working
     */
    public function setWorking()
    {
        $this->isWorking = true;
    }

    /**
     *  Set worker state to not working
     */
    public function setNotWorking()
    {
        $this->isWorking = false;
    }

    public function hasJob()
    {
        return !is_null($this->currentJob);
    }

    /**
     * @param JobInterface $job
     * @throws \LogicException
     * @return void
     */
    public function setCurrentJob(JobInterface $job)
    {
        if ($this->hasJob()) {
            throw new \LogicException('Worker has already a job');
        }

        $this->currentJob = $job;
    }

    /**
     * @return JobInterface
     */
    public function getCurrentJob()
    {
        return $this->currentJob;
    }

    /**
     * @return void
     */
    public function removeCurrentJob()
    {
        $this->currentJob = null;
    }


    /**
     * Shutdown the worker
     *
     * @param \ZMQSocket $socket
     * @return void
     */
    public function shutdown(\ZMQSocket $socket)
    {
        $socket->send($this->getWorkerAddress(), \ZMQ::MODE_SNDMORE);
        $socket->send("", \ZMQ::MODE_SNDMORE);
        $socket->send(MessageBroker::WORKER_COMMAND_SHUTDOWN);
    }

    public function requestStatus(\ZMQSocket $socket) {
        $socket->send($this->getWorkerAddress(), \ZMQ::MODE_SNDMORE);
        $socket->send("", \ZMQ::MODE_SNDMORE);
        $socket->send(MessageBroker::WORKER_COMMAND_STATUS);
    }

    /**
     * @return bool
     */
    public function hasCloseRequest()
    {
        return $this->closeRequest;
    }

    public function closeRequest()
    {
        $this->closeRequest = true;
    }

    /**
     * Set worker in waiting mode for next ping
     * @return void
     */
    public function setWaitingForNextPing()
    {
        $this->waitingForPing = true;
    }

    /**
     * Get worker ping state
     *
     * @return bool
     */
    public function isWaitingForNextPing()
    {
        return $this->waitingForPing;
    }

    /**
     * Receive and check a empty frame
     *
     * @param $socket
     * @throws \LogicException
     */
    protected function emptyFrame($socket)
    {
        $empty = $socket->recv();

        if (!empty($empty)) {
            throw new \LogicException('Expected empty frame, but receive none empty frame');
        }
    }

    /**
     * @param \ZMQSocket $socket
     * @param JobInterface $job
     * @return void
     */
    public function handleJob(\ZMQSocket $socket, JobInterface $job)
    {
        $this->setCurrentJob($job);
        $this->setLastPing();
        $this->setWorking();

        $socket->send($this->getWorkerAddress(), \ZMQ::MODE_SNDMORE);
        $socket->send("", \ZMQ::MODE_SNDMORE);
        $socket->send(MessageBroker::WOKRER_COMMAND_HANDLEJOB, \ZMQ::MODE_SNDMORE);
        $socket->send("", \ZMQ::MODE_SNDMORE);
        $socket->send(json_encode($job->getJobData()));
    }

    public function onJobComplete($command, \ZMQSocket $socket)
    {
        $this->emptyFrame($socket);

        $status = $socket->recv();

        $this->removeCurrentJob();
        $this->setLastPing();
        $this->setNotWorking();

        if ($status == MessageBroker::WORKER_RESPONSE_JOB_SUCCESS) {
            $this->log('Job was successfully');
            return true;
        }

        $this->log('Job has failed', 'error');
        return false;
    }

    public function onStatusUpdate($command, \ZMQSocket $socket) {
        $this->emptyFrame($socket);
        $data = $socket->recv();

        $data = json_decode($data, true);

        if(is_array($data)) {
            $this->status = array(
                'memory_usage'  => !empty($data['memory_usage']) ? $data['memory_usage'] : 0,
                'memory_peak'   => !empty($data['memory_peak']) ? $data['memory_peak'] : 0,
                'runtime'       => !empty($data['runtime']) ? $data['runtime'] : 0,
                'type'          => !empty($data['type']) ? $data['type'] : $this->getType(),
                'ip'            => !empty($data['ip']) ? $data['ip'] : '0.0.0.0',
                'os'            => !empty($data['os']) ? $data['os'] : 'Unknown',
                'extras'        => !empty($data['extras']) ? $data['extras'] : 'null',
            );

            foreach($this->status as $key => $value) {
                $this->log(sprintf('%s : %s', $key, $value));
            }

        } else {
            $this->status = array();
        }

        return true;
    }

    /**
     * Called when worker has unsubscribe from router
     *
     * @return void
     */
    public function onUnsubscribe()
    {
        return;
    }

    /**
     * Return true, if worker support status command
     *
     * @return bool
     */
    public function canReceiveStatusCommand()
    {
        return true;
    }
}
