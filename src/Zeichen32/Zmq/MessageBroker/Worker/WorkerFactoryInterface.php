<?php
/**
 * Created by PhpStorm.
 * User: jens
 * Date: 30.09.13
 * Time: 12:01
 */

namespace Zeichen32\Zmq\MessageBroker\Worker;


interface WorkerFactoryInterface {
    /**
     * Return Worker Type
     *
     * @return string
     */
    static public function getType();

    /**
     * Create a new Worker
     *
     * @param $workerAddress
     * @return WorkerInterface
     */
    public function create($workerAddress);

    /**
     * Create a new Worker
     *
     * @param $workerAddress
     * @return WorkerInterface
     */
    function __invoke($workerAddress);
} 