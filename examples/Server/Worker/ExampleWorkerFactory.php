<?php

class ExampleWorkerFactory implements \Zeichen32\Zmq\MessageBroker\Worker\WorkerFactoryInterface {

    /**
     * Return Worker Type
     *
     * @return string
     */
    static public function getType()
    {
        return 'EXAMPLE_WORKER';
    }

    /**
     * Create a new Worker
     *
     * @param $workerAddress
     * @return \Zeichen32\Zmq\MessageBroker\Worker\WorkerInterface
     */
    public function create($workerAddress)
    {
        return new ExampleWorker($workerAddress);
    }

    /**
     * Create a new Worker
     *
     * @param $workerAddress
     * @return \Zeichen32\Zmq\MessageBroker\Worker\WorkerInterface
     */
    function __invoke($workerAddress)
    {
        return $this->create($workerAddress);
    }


} 