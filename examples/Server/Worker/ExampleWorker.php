<?php

class ExampleWorker extends \Zeichen32\Zmq\MessageBroker\Worker\AbstractWorker
{

    /**
     * Return custom Commands
     *
     * @return array
     */
    public function getCommands()
    {

        // You can add custom commands to handle complex jobs
        // or leave everything as it is
        return array_merge(
            parent::getCommands(),
            array()
        );
    }

    /**
     * Return Worker type
     *
     * @return string
     */
    public function getType()
    {
        return 'EXAMPLE_WORKER';
    }

    /**
     * Check if a job is supported by worker
     *
     * @param \Zeichen32\Zmq\MessageBroker\Job\JobInterface $job
     * @return bool
     */
    public function support(\Zeichen32\Zmq\MessageBroker\Job\JobInterface $job)
    {

        // If your worker support this type of job you must return true
        return ($job->getType() == 'EXAMPLE_JOB');
    }
}
