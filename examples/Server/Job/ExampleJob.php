<?php
/**
 * Class ExampleJob
 */
class ExampleJob extends \Zeichen32\Zmq\MessageBroker\Job\AbstractJob
{

    /**
     * Return job type
     *
     * @return string
     */
    public function getType()
    {
        return 'EXAMPLE_JOB';
    }

    public function hasExceededTTL()
    {
        // In standard mode, we gave a job 3 retries to execute
        // Feel free to edit this method
        return parent::hasExceededTTL();
    }


    /**
     * Return true if job is ready to work
     *
     * @return bool
     */
    public function isReady()
    {

        // Sometimes a job is not ready or is no longer valid.
        // Return false to suspend job for a while

        // Run job on second try
        if ($this->getTTL() > 1) {
            return true;
        }

        // You can use logger
        $this->log('Job is not ready jet');

        return false;
    }
}
