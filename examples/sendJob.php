<?php
$loader = require_once __DIR__ . '/../vendor/autoload.php';

// In you normal application,
// you can any job by call this static method
\Zeichen32\Zmq\Client\Client::callMessageBroker('EXAMPLE_JOB', array('data' => 'Some additional data'));
