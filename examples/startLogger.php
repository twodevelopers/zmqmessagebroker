<?php
$loader = require_once __DIR__ . '/../vendor/autoload.php';

use Zeichen32\Zmq\Worker\LogMonitorWorker;

$worker = new LogMonitorWorker(array('server' => '127.0.0.1'));

$worker->setLogger(new \Zeichen32\Zmq\Logger\EchoLogger());

// Connect worker to message broker
$worker->connect();
