<?php
$loader = require_once __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/Server/Job/ExampleJob.php';
require_once __DIR__ . '/Server/Worker/ExampleWorker.php';
require_once __DIR__ . '/Server/Worker/ExampleWorkerFactory.php';

$context = new \ZMQContext();

// Router für die Clients (Web-Anwendung)
$frontend = new \ZMQSocket($context, \ZMQ::SOCKET_ROUTER);

// Router für die Worker
$backend = new \ZMQSocket($context, \ZMQ::SOCKET_ROUTER);

// Configure ip adresses for clients and worker
$config = array(
    'ipAddressFrontend' => '127.0.0.1',
    'ipAddressBackend' => '127.0.0.1',
);

$broker = new \Zeichen32\Zmq\MessageBroker\MessageBrokerLogEnable($frontend, $backend, $config);

// Add you worker types with Factory class oder Closure function
$broker->addWorkerTypeFactory(new ExampleWorkerFactory());

// Add your job types with Factory class oder Closure function
$broker->addJobType('EXAMPLE_JOB', function ($clientAddress) {
    return new ExampleJob($clientAddress);
});

// Set a example logger. You can leave this...
// You can add classes with implements LoggerInterface or callback function
$broker->setLogger(new \Zeichen32\Zmq\Logger\EchoLogger());

// Start message broker
$broker->run();
