<?php

class ExampleClientWorker extends \Zeichen32\Zmq\Worker\AbstractWorker
{

    /**
     * Override this method to execute custom commands
     *
     * @param $command
     * @param \ZMQSocket $messageBroker
     * @return mixed
     */
    protected function handleCommand($command, \ZMQSocket $messageBroker)
    {
        // Most of default methods are already implement. If you need some custom
        // commands, you can handle this commands in is method
        return false;
    }

    /**
     * Override this method to handle a job request
     *
     * @param array $data
     * @param \ZMQSocket $messageBroker
     * @return bool
     */
    protected function handleJobCommand(array $data, \ZMQSocket $messageBroker)
    {
        // In this method, the work will be done
        // Return true if the job was successfully run
        // Return false if the job has been failed

        for ($i = 0; $i < 10; $i++) {
            $this->log(sprintf('Do work (%d)...', $i));
            sleep(1);
        }

        return true;
    }

    public function getType()
    {
        return 'EXAMPLE_WORKER';
    }
}
